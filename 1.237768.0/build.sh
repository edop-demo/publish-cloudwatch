#!/bin/bash

# Note: The version number for this app came from the image build output:
#   Unpacking amazon-cloudwatch-agent (1.237768.0-1) ...
#   Setting up amazon-cloudwatch-agent (1.237768.0-1) ...

set -e

# Dev
aws ecr get-login-password --profile=chc-platform --region us-east-1 | docker login --username AWS --password-stdin 668358644152.dkr.ecr.us-east-1.amazonaws.com/imnplatform-dev-platform-cloudwatchagent-v2
docker build -t 668358644152.dkr.ecr.us-east-1.amazonaws.com/imnplatform-dev-platform-cloudwatchagent-v2:1.237768.0 .
docker push 668358644152.dkr.ecr.us-east-1.amazonaws.com/imnplatform-dev-platform-cloudwatchagent-v2:1.237768.0

exit 0