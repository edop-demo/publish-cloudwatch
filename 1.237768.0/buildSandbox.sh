#!/bin/bash

# Note: The version number for this app came from the image build output:
#   Unpacking amazon-cloudwatch-agent (1.237768.0-1) ...
#   Setting up amazon-cloudwatch-agent (1.237768.0-1) ...

set -e

# Sandbox

aws ecr get-login-password --profile=chc-sandbox --region us-east-1 | docker login --username AWS --password-stdin 380685215783.dkr.ecr.us-east-1.amazonaws.com/imnsample-cloudwatchagent

docker build -t 380685215783.dkr.ecr.us-east-1.amazonaws.com/imnsample-cloudwatchagent:1.237768.0 .

docker push 380685215783.dkr.ecr.us-east-1.amazonaws.com/imnsample-cloudwatchagent:1.237768.0

exit 0