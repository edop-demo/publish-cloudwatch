# publish-cloudwatch

Sidecar Image that runs the Cloudwatch Agent.

# Project Layout

```bash
.
├── 1.xx.x.x                    - sidecar version 1.xx.x.x
│   ├── Dockerfile              - Dockerfile to build the images for this project
└── README.md                   - this file
├── .gitlab-ci.yml              - CI file that creats gitlab pipeline
├── .gitignore                  - Files to be ignored when commiting to git
├── variables.yml               - Project specific variables used in CICD pipeline jobs.
```

# Note: 
Refer to pipelines common repo - https://gitlab.com/edop-demo/pipelines-common for CICD pipeline code.  

Original Instructions:
- https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/ContainerInsights-build-docker-image.html

Dockerfile:
- https://github.com/aws-samples/amazon-cloudwatch-container-insights/blob/master/cloudwatch-agent-dockerfile/Dockerfile

This dockerfile was modified in the following ways, to make it work:
- changed to alpine image so that we could use /bin/sh
- created the directories that cwagent expected to be present already
- updated the permissions on the directories so that the cwagent could actually write the files it needs
